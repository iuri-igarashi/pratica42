
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica42 {
    public static void main(String[] args) {
        Elipse a = new Elipse(5,2);
        Circulo b = new Circulo(5);
        
        System.out.println(a.getArea());
        System.out.println(a.getPerimetro());
        
        System.out.println(b.getArea());
        System.out.println(b.getPerimetro());
        
    }
}
